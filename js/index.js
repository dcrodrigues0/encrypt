var botaoCriptografar = document.querySelector('#btn-criptografar');
botaoCriptografar.addEventListener('click', function (event) {

	event.preventDefault();

	var inputTextoDescriptografado = document.querySelector('#texto-descriptografado');
	var textoCriptografadoList = document.querySelector("#textoCriptografadoList");
	var inputChavePublica = document.querySelector('#public-key');
	var inputTextoCriptografado = document.querySelector("#texto-criptografado");

	var textoDescriptografado = inputTextoDescriptografado.value;


	if (validaChavePublica(inputChavePublica)) {

		//criptografar
		var sequenciaDeCaracteres = criptografar(inputTextoDescriptografado.value,alfabeto);
		var alfabetoCriptografado = sequenciaDeCaracteres.join('');
		textoCriptografadoList.value = sequenciaDeCaracteres;
		inputTextoCriptografado.value = alfabetoCriptografado; 
		inputTextoDescriptografado.value = "";
		var erro = document.querySelector("#mensagem-erro");
		erro.innerHTML = ""


		
	}else{
		var erro = document.querySelector("#mensagem-erro");
		erro.innerHTML = "Chave Pública Inválida!"
		inputTextoDescriptografado.value = "";
	}

})


function validaChavePublica(inputChavePublica) {
	var chavePublica = "teste123"
	if (inputChavePublica.value == chavePublica) {
		return true;
	} else{
		return false;
	}
}

