function descriptografar(textoCriptografadoList,alfabeto) {	
	
	var textDescriptografado = "";
	textoCriptografadoList = textoCriptografadoList.split(",")

	console.log(textoCriptografadoList);
	textoCriptografadoList.forEach(function(letraCriptografada){
		textDescriptografado += getKeyByValue(alfabeto, letraCriptografada);
	})

	return textDescriptografado;
}

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

//descriptografar(inputTextoCriptografado,alfabeto)