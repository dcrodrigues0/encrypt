var alfabeto = {
	a: "##æßðħð",
	b: "@@@",
	c: "poc",
	d: "bik",
	e: "asd8",
	f: "asd7ya",
	g: "asygd",
	h: "asudo",
	i: "uas7d9",
	j: "jaudsh",
	k: "asdaª",
	l: "ajskpº",
	m: "skua",
	n: "sgat",
	o: "syuai",
	p: "as6",
	q: "asuid",
	r: "asuihd",
	s: "ausid",
	t: "aisjdu",
	u: "asygasdªºd",
	v: "ashud",
	w: "a90m",
	x: "dy78a",
	y: "fw98u",
	z: "jf9w2",
	ç: "kd85º",
	" ": ""
}

function criptografar(inputTextoDescriptografado,alfabeto) {	
	
	var sequenciaDeCaracteres = [];

	for(i = 0; i < inputTextoDescriptografado.length; i++){
		sequenciaDeCaracteres.push(alfabeto[inputTextoDescriptografado[i]]);
	}

	//var alfabetoCriptografado = sequenciaDeCaracteres.join('');
	//console.log(inputTextoDescriptografado)

	//return alfabetoCriptografado;
	return sequenciaDeCaracteres;
}